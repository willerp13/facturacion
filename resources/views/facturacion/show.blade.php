@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                   codigo de la factura: 000{{$datas->codigo_factura}}
                   <br>
                   <br>
                   
                   <br>
                   <br>
                   <br>
                   @php
                   $sumar=null;
                   @endphp
                  
                                        <div class="row d-flex justify-content-center">
                                        @foreach($productos as $producto)
                                            <div class="col-8">
                                                <div>producto: {{$producto['producto']}} - costo: {{$producto['precio']}} + Iva: {{$producto['porcentaje']}}% | precio real: @php $imprimir = $producto['precio'] + ($producto['precio']*($producto['porcentaje']/100) ); echo $imprimir; $sumar+=$imprimir; @endphp</td></div>
                                                <br>
                                            </div>
                                        @endforeach
                                        <div class="col-5">
                                            @php
                                            echo "total a pagar: ".$sumar;
                                            @endphp
                                        </div>
                                        </div>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection