@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    @if (Auth::user()->status == 2)
                        <div> {{ __($mensaje) }}</div> 


                            <form action="{{ route('compras.store') }}" method="post">
                            @csrf
                                <select class="form-select" name="compraritem" id="compraritem">
                                    @foreach($productos as $producto)
                                        <option value="{{ json_encode(['name'=> $producto->name, 'codigo'=> $producto->codigo, 'id_producto' => $producto->id, 'porcentaje' => $producto->porcentaje,  'precio' => $producto->precio]) }}">{{ $producto->name }} - codigo: {{ $producto->codigo }}</option>
                                    @endforeach
                                </select>
                                <button type="submit">Guardar</button>
                            </form>

                            <div>
                                @if (!empty($datas))
                                        @foreach($datas as $data)
                                        <div class="row d-flex justify-content-center">
                                            <div class="col-4">
                                            {{ $data->id }} - <div>{{ $data->producto }} - codigo: {{ $data->codigo_producto }} - porcentaje: {{$data->porcentaje}} -  </td></div>
                                                <br>
                                            </div>
                                        </div>
                                        @endforeach
                                @endif
                            </div>
                    @else
                            <form action="{{ route('facturar.store') }}" method="post">
                            @csrf
                                <div> <button>facturar</button></div>  
                            </form>
                    @endif
                   

                    @if (!empty($datas) && Auth::user()->status == 1)
                            @foreach($datas as $data)
                            <div class="row d-flex justify-content-center">
                                <div class="col-4">
                                   id - {{$data->id}}<div>-codigo factura: 000{{ $data->codigo_factura }} </div><a href="{{ route('factura.show', $data->id) }}" class="btn btn-info btn-sm">verificar factura</a> 
                                    <br>
                                </div>
                            </div>
                            @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
