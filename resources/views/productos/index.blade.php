@extends('layouts.app')

@section('content')
<div>
<form method="post" action="{{ url('productos-store') }}">
@csrf
    <div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-4">
            Producto <input type="text" name="producto" id="producto" placeholder="ingrese el nombre del producto"></br></br>
            Codigo <input type="text" name="codigo" id="codigo" placeholder="ingrese el codigo del producto"></br></br>
            precio <input type="text" name="precio" id="precio" placeholder="ingrese el precio"></br></br>
            Porcentaje %<input type="number" name="porcentaje" id="porcentaje" placeholder="ingrese el porcentaje del producto"></br></br>
        <button type="submit">Guardar</button>

        </div>
    </div>
    </div>

    @if (!empty($datas))
        @foreach($datas as $data)
        <div class="row d-flex justify-content-center">
            <div class="col-4">
                <div>{{ $data->id }} - {{ $data->name }} - codigo: {{ $data->codigo }} - porcentaje: %{{$data->porcentaje}} - <a href="{{ route('productos.show', $data->id) }}" class="btn btn-info btn-sm">Editar</a> - <a href="{{ route('productos.destroy', $data->id) }}" class="btn btn-info btn-sm">borrar</a></td></div>
                <br>
            </div>
        </div>
        @endforeach
   @endif


</form>
</div>
@endsection