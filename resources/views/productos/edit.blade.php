@extends('layouts.app')

@section('content')
<div>
<form method="post" action="{{ route('productos.update', $datas->id) }}">
@csrf
    <div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-4">
        Producto <input type="text" name="producto" id="producto" value="{{$datas->name}}" placeholder="ingrese el nombre del producto"></br></br>
        Codigo <input type="text" name="codigo" id="codigo" value="{{$datas->codigo}}" placeholder="ingrese el codigo del producto"></br></br>
        precio <input type="text" name="precio" id="precio" value="{{$datas->precio}}" placeholder="ingrese el precio"></br></br>
        Porcentaje %<input type="number" name="porcentaje" id="porcentaje" value="{{$datas->porcentaje}}" placeholder="ingrese el porcentaje del producto"></br></br>
        <button type="submit">Guardar</button>

        </div>
    </div>
    </div>



</form>
</div>
@endsection