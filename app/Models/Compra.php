<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Compra extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'producto',
        'codigo_producto',
        'id_user',
        'porcentaje',
        'id_producto',
        'precio',
        'facturacion',
    ];
}
