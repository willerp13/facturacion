<?php

namespace App\Http\Controllers;

use App\Models\Factura;
use App\Models\Compra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class FacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) //facturar
    {
        
        $users = Compra::all();
        $groupedUsers = $users->groupBy('id_user');
        
        foreach($groupedUsers as $registros) {

            $Store = new Factura([
                'concepto' => $registros,
                'codigo_factura' => Factura::max('id')+1,
            ]);
    
            $Store->save();
        }
        Compra::query()->delete();

        $elementos = Factura::all();

        
        return view('home', [
            'datas' => $elementos,
        ]);  

        return view('facturacion.index', [
            'datas' =>$groupedUsers,
        ]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $elementos = Factura::find($id); //llamamos a todos los resultados de la tabla factura por el id
        $productos = $elementos->concepto; //seleccionamos el concepto
        $productos2 = json_decode($productos, true); //le pasamos un jsondecode para convertir un string de JSON en un array
        return view('facturacion.show', [
            'datas' => $elementos,
            'productos' => $productos2,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function edit(Factura $factura)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Factura $factura)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function destroy(Factura $factura)
    {
        //
    }
}
