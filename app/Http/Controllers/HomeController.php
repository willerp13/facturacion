<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Compra;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->status==2){
            $elementos = Compra::where("id_user",auth()->id())->get();
            //return $elementos[1];

            return view('home', [
                'productos' => Producto::all(),
                'datas' => $elementos,
                'mensaje' => 'eres un invitado'
            ]);
        }else{
            return view('home',['mensaje' => 'has click aqui para cerrar facturas abiertas -> ']);
        }
        
    }
}
