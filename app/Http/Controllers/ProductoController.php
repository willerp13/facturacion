<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //$userId = Auth::id();
        //return dd($userId);
        
        $elementos = Producto::all();

       
        return view('productos.index', [
            'datas' => $elementos,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'producto' => 'required',
            'codigo' => 'required',
            'porcentaje' => 'required',
            'precio' => 'required',
            ]);

        $producto = $request->get('producto');
        $codigo = $request->get('codigo');
        $porcentaje = $request->get('porcentaje');
        $precio = $request->get('precio');

        $Store = new Producto([
            'name' => $producto,
            'codigo' => $codigo,
            'porcentaje' => $porcentaje,
            'precio' => $precio
            
        ]);

        $Store->save();

        $elementos = Producto::all();

        
        return  Redirect::to('productos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $elementos = Producto::find($id);
        
        return view('productos.edit', [
            'datas' => $elementos,
        ]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
            $data = Producto::find($id);
            $data->name = $request->get('producto');
            $data->codigo = $request->get('codigo');
            $data->precio = $request->get('precio');
            $data->porcentaje = $request->get('porcentaje');
            $data->save();
            return redirect()->route('productos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
            $producto = Producto::find($id);
            $producto->delete($id);
            return redirect()->route('productos')
            ->with('success','Company has been deleted successfully');
    }
}
