<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


//rutas productos
Route::get('/productos', [App\Http\Controllers\ProductoController::class, 'index'])->name('productos');

Route::post('/productos-store', [App\Http\Controllers\ProductoController::class, 'store'])->name('productos.store');

Route::get('/productos-edit/{id}', [App\Http\Controllers\ProductoController::class, 'show'])->name('productos.show');

Route::post('/productos-update/{id}', [App\Http\Controllers\ProductoController::class, 'update'])->name('productos.update');

Route::get('/productos-destroy/{id}', [App\Http\Controllers\ProductoController::class, 'destroy'])->name('productos.destroy');

//rutas compras
Route::post('/compras-store', [App\Http\Controllers\CompraController::class, 'store'])->name('compras.store');

//facturacion
Route::post('/facturar', [App\Http\Controllers\FacturaController::class, 'store'])->name('facturar.store');

Route::get('/factura-show/{id}', [App\Http\Controllers\FacturaController::class, 'show'])->name('factura.show');
