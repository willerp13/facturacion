<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Producto;

class ProductosInit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Producto::create([
            'name' => 'pollo',
            'codigo' => '0001',
            'precio' => 123.45,
            'porcentaje'=> 5,
        ]);
        Producto::create([
            'name' => 'carne',
            'codigo' => '0002',
            'precio' => 45.65,
            'porcentaje'=> 15,
        ]);
        Producto::create([
            'name' => 'pescado',
            'codigo' => '0003',
            'precio' => 39.73,
            'porcentaje'=> 12,
        ]);
        Producto::create([
            'name' => 'salchichas',
            'codigo' => '0004',
            'precio' => 250.00,
            'porcentaje'=> 8,
        ]);
        Producto::create([
            'name' => 'papas',
            'codigo' => '0005',
            'precio' => 59.35,
            'porcentaje'=> 10,
        ]);
    }
}
