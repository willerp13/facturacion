<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsuarioAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make(12345678),
            'status'=> 1,
        ]);
        User::create([
            'name' => 'prueba1',
            'email' => 'prueba1@prueba1.com',
            'password' => Hash::make(12345678),
            'status'=> 2,
        ]);
        User::create([
            'name' => 'prueba2',
            'email' => 'prueba2@prueba2.com',
            'password' => Hash::make(12345678),
            'status'=> 2,
        ]);
        User::create([
            'name' => 'prueba3',
            'email' => 'prueba3@prueba3.com',
            'password' => Hash::make(12345678),
            'status'=> 2,
        ]);
    }
}
